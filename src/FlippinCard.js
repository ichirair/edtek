import React, { useState } from "react";
import { Row, Col, Card, Container } from "react-bootstrap";
import ReactCardFlip from "react-card-flip";

const FlippingCards = () => {
  const [isFlipped, setIsFlipped] = useState([false, false, false]);

  const handleClick = (index) => {
    setIsFlipped((prevState) => {
      const newState = [...prevState];
      newState[index] = !newState[index];
      return newState;
    });
  };

  return (
    <div className="services" id="services">
      <Container>
        <h3 style={{ fontWeight: "bold", color: "#0F0C1F" }} className="mb-5">
          Our Services
        </h3>
        <Row className="mt-3 mb-5">
          {/* Development Card */}
          <Col sm={4}>
            <ReactCardFlip isFlipped={isFlipped[0]} flipDirection="horizontal">
              {/* Front Side */}
              <Card
                onClick={() => handleClick(0)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body style={{ padding: "10px" }}>
                  <img
                    src={process.env.PUBLIC_URL + "/software.svg"}
                    alt="Technology Development"
                    style={{ width: "15rem", marginTop: "2rem" }}
                  />
                  <h3
                    style={{
                      fontSize: "1.5rem",
                      fontWeight: "bold",
                      marginBottom: "10px",
                      textAlign: "center",
                    }}
                    className="mt-5">
                    Development
                  </h3>
                  <p
                    style={{
                      color: "#777",
                      marginBottom: "10px",
                      textAlign: "justify",
                      marginTop: "1.5rem",
                    }}>
                    We offer cutting-edge Full Stack Software Development to
                    build robust applications, transformative IoT solutions to
                    connect the world, and advanced AI-DDS technology to drive
                    intelligent innovation.
                  </p>

                  <div
                    style={{
                      backgroundColor: "#6936F4",
                      borderRadius: "25px",
                      color: "#fff",
                      fontSize: "small",
                      display: "inline-block",
                      marginTop: "3rem",
                      width: "50%",
                      padding: "3px",
                    }}>
                    Learn More
                  </div>
                </Card.Body>
              </Card>

              {/* Back Side */}
              <Card
                onClick={() => handleClick(0)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body>
                  <Card.Title>Development & Products</Card.Title>
                  <Card.Text>
                    <strong>Software Development:</strong> We create custom
                    software tailored to your needs, enhancing efficiency and
                    keeping you competitive.
                    <br />
                    <br />
                    <strong>E-Waste Management:</strong> Our eBin solution
                    ensures responsible disposal and recycling of electronics,
                    supporting sustainability.
                    <br />
                    <br />
                    <strong>System Design:</strong> We design innovative
                    products using the latest technology, backed by thorough
                    research and detailed white papers.
                  </Card.Text>
                </Card.Body>
              </Card>
            </ReactCardFlip>
          </Col>

          {/* Consulting Card */}
          <Col sm={4}>
            <ReactCardFlip isFlipped={isFlipped[1]} flipDirection="horizontal">
              {/* Front Side */}
              <Card
                onClick={() => handleClick(1)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body style={{ padding: "10px" }}>
                  <img
                    src={process.env.PUBLIC_URL + "/consulting.svg"}
                    alt="Consulting"
                    style={{ width: "11rem", marginTop: "2rem" }}
                  />
                  <h3
                    style={{
                      fontSize: "1.5rem",
                      fontWeight: "bold",
                      marginBottom: "10px",
                      textAlign: "center",
                    }}
                    className="mt-5">
                    Consulting
                  </h3>
                  <p
                    style={{
                      color: "#777",
                      marginBottom: "10px",
                      textAlign: "justify",
                      marginTop: "1.5rem",
                    }}>
                    We provide expert consulting on Software Development, IoT,
                    and AI to enhance your technology strategies and drive
                    growth.
                  </p>

                  <div
                    style={{
                      backgroundColor: "#6936F4",
                      borderRadius: "25px",
                      color: "#fff",
                      fontSize: "small",
                      display: "inline-block",
                      marginTop: "3rem",
                      width: "50%",
                      padding: "3px",
                    }}>
                    Learn More
                  </div>
                </Card.Body>
              </Card>

              {/* Back Side */}
              <Card
                onClick={() => handleClick(1)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body>
                  <Card.Title>Consulting</Card.Title>
                  <Card.Title>Consulting</Card.Title>
                  <Card.Text>
                    <strong>Digital Transformation:</strong> Leverage technology
                    to fundamentally change how businesses operate and deliver
                    value. Our experts identify opportunities for digital
                    transformation, design new processes, and drive innovation
                    and growth.
                    <br />
                    <br />
                    <strong>Insights and Analytics:</strong> Make informed
                    business decisions and stay ahead of the competition with
                    data analytics and insights. Our team specializes in data
                    strategy development, data warehousing, integration,
                    visualization, and advanced analytics.
                    <br />
                    <br />
                    <strong>Cloud Services:</strong> Optimize your use of cloud
                    technology with our expertise in cloud infrastructure,
                    migration, security, and management.
                  </Card.Text>
                </Card.Body>
              </Card>
            </ReactCardFlip>
          </Col>

          {/* Research Card */}
          <Col sm={4}>
            <ReactCardFlip isFlipped={isFlipped[2]} flipDirection="horizontal">
              {/* Front Side */}
              <Card
                onClick={() => handleClick(2)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body style={{ padding: "10px" }}>
                  <img
                    src={process.env.PUBLIC_URL + "/research.svg"}
                    alt="Research"
                    style={{ width: "12rem", marginTop: "2rem" }}
                  />
                  <h3
                    style={{
                      fontSize: "1.5rem",
                      fontWeight: "bold",
                      marginBottom: "10px",
                      textAlign: "center",
                    }}
                    className="mt-5">
                    Research
                  </h3>
                  <p
                    style={{
                      color: "#777",
                      marginBottom: "10px",
                      textAlign: "justify",
                      marginTop: "1.5rem",
                    }}>
                    We lead technological research in AI, IoT, VR, and AR to
                    drive innovation and advancement.
                  </p>

                  <div
                    style={{
                      backgroundColor: "#6936F4",
                      borderRadius: "25px",
                      color: "#fff",
                      fontSize: "small",
                      display: "inline-block",
                      marginTop: "3rem",
                      width: "50%",
                      padding: "3px",
                    }}>
                    Learn More
                  </div>
                </Card.Body>
              </Card>

              {/* Back Side */}
              <Card
                onClick={() => handleClick(2)}
                className="flipping-card"
                style={{
                  maxHeight: "35rem",
                  minHeight: "35rem",
                  overflowY: "scroll",
                }}>
                <Card.Body>
                  <Card.Title>Research & Innovation</Card.Title>
                  <Card.Text>
                    We focus on developing solutions for key challenges through
                    systematic research, aiming for impactful and sustainable
                    outcomes.
                  </Card.Text>
                  <Card.Title>Skills Development</Card.Title>
                  <Card.Text>
                    Our eBin solution promotes responsible e-waste recycling,
                    supporting environmental sustainability.
                  </Card.Text>
                  <Card.Title>Products</Card.Title>
                  <Card.Text>
                    We design innovative, industry-specific products using
                    advanced technology, backed by thorough research and
                    detailed documentation.
                  </Card.Text>
                </Card.Body>
              </Card>
            </ReactCardFlip>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default FlippingCards;
