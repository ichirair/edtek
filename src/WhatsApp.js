import React from "react";

const WhatsAppButton = () => {
  const whatsappNumber = "27635897747";
  const whatsappUrl = `https://wa.me/${whatsappNumber}`;

  return (
    <a
      href={whatsappUrl}
      target="_blank"
      rel="noopener noreferrer"
      className="whatsapp-button">
      <img
        src={`${process.env.PUBLIC_URL}/whatsapp.png`}
        alt="WhatsApp"
        className="circular-image"
      />
    </a>
  );
};

export default WhatsAppButton;
