import React from "react";
import { Container, Row, Col } from "react-bootstrap";

const Services = () => {
  const products = [1, 2, 3, 4, 5];

  return (
    <div className="services">
      <div
        style={{
          background: "rgb(15, 12, 31)",
          padding: "3rem 2rem 6rem",
        }}
        className="mt-5">
        <h3 className="mt-3 gradient-text" style={{ fontWeight: "bold" }}>
          In-House Products
        </h3>
        <Row className="mt-4 justify-content-center">
          {products.map((product) => (
            <Col
              key={product}
              xs={6}
              sm={4}
              md={2}
              className="d-flex justify-content-center align-items-center">
              <div
                style={{
                  height: "300px",
                  display: "flex",
                  alignItems: "center",
                }}>
                <img
                  src={`${process.env.PUBLIC_URL}/Products/${product}.svg`}
                  alt={`Product ${product}`}
                  style={{
                    maxWidth: "100%",
                    maxHeight: "100%",
                    objectFit: "contain",
                  }}
                />
              </div>
            </Col>
          ))}
        </Row>
      </div>
    </div>
  );
};

export default Services;
